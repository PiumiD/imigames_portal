<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = 'games';
    protected $primaryKey = 'game_id';

    public function comments() {
        return $this->hasMany('App\Comment');
    }
    public function score() {
        return $this->hasMany('App\Leader_Board');
    }
}
