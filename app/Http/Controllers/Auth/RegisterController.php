<?php

namespace App\Http\Controllers\Auth;

use App\socialProvider;
use App\User;
use Laravel\Socialite\Facades\Socialite;
use Mockery\CountValidator\Exception;


use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\Auth\File;
use Illuminate\Support\Facades\Input;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $file= Input::file('pc');

        if($file!=null) {


            $name = $file->getClientOriginalName();
            //dd($name);
            $file->move(public_path() . '/images/', $name);


            return User::create([
                'user_name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'profile_pic' => ("images/".$name),

            ]);
        }

        else{

            return User::create([
                'user_name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'profile_pic' => 'images/default.png',

            ]);
        }
    }

    public function redirectToProvider($provider)
    {

        return Socialite::driver($provider)->redirect();

    }


    public function handleProviderCallback($provider)
    {



        try {

            $socialuser = Socialite::driver($provider)->user();

            //return $socialuser('/');
        }
        catch(Exception $e){
            return redirect('/');


        }

        $userprovider = socialProvider::Where('provider_id',$socialuser->getId())->first();


        if(!$userprovider){
           // dd('11');

            $newuser = new User();
            // dd($newuser);
            $newuser->user_name = $socialuser->getName();
            $newuser->email = $socialuser->getEmail();
            $newuser->profile_pic = $socialuser->getAvatar();
            $newuser->save();


            //create the new user and provider
//            $user = User::firstOrCreate(
//
//
//                ['email'=>$socialuser->getEmail()],
//                ['user_name'=>$socialuser->getName()]
//
//
//
//            );
            $newuser-> socialProviders()->create(
                ['provider_id'=>$socialuser->getId(),'provider'=>$provider]



            );





        }
        else{

            $newuser=  $userprovider->user;
            auth()->login($newuser);
            return redirect()->to('/first');



        }

        //dd('222');
        auth()->login($newuser);
        return redirect()->to('/first');


    }









}
