<?php

namespace App\Http\Controllers;
use App\Events\commentadded;
use App\Comment;
use App\Like;
use Illuminate\Http\Request;

class Comment_LikeController extends Controller
{

    public function handle(Request $request)
    {
        $u = $request->input( 'user_id' );
        $g = $request->input( 'game_id' );
        $c = $request->input( 'comment' );

        // $comment = new Comment();
        $comment = new Comment();
        $comment->user_id = $u;
        $comment->game_id = $g;
        $comment->comment = $c;
        $comment->save();
        $id =$comment-> id;
        $order = Comment::findOrFail($id);
        event(new commentadded($order));

    }










    public function add_comment(Request $request){
        $com = new Comment;
        $com->game_id = $request->game_id;
        $com->user_id = $request->user_id;
        $com->comment  = $request->comment ;
        $com->save();
    }
    public function delete_comment(Request $request){

        if(Comment::where('user_id', $request->input('id'))->delete()){

            return "SUCCESS";

        } else {

            return "FAIL";
        }

    }

    public function edit_comment($comment_id)
    {
dd('done');
        Comment::where(['comment_id',$comment_id] )->delete() ;

            return "SUCCESS";


    }
    public function show_comment(Request $request){

        $coms =Comment::with(['user','game'])->where("game_id",$request->input('game_id'))->get();
       // dd($coms);
        return view('test.comment_show')
            ->with(['coms'=>$coms]);
    }

    public function add_like(Request $request){
        $like = new Like;
        $like->like_id = $request->like_id;
        $like->game_id = $request->game_id;
        $like->user_id = $request->user_id;
        $like->no_like = $request->no_like;
        $like->save();
    }
    public function un_like(Request $request){
        if(Like::where('user_id', $request->input('id'))->delete()){

            return "SUCCESS";

        } else{

            return "FAIL";
        }
    }
}
