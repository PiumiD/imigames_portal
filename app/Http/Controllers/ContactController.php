<?php

namespace App\Http\Controllers;

use App\Contacts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContactController extends Controller
{
public function save_con(Request $request)
{
   if( Auth::guest()){}
  // auth::guest();
    $con = new Contacts();
    $con->name = $request->name;
    $con->email = $request->email;
    $con->tel_no = $request->tel_no;
    $con->message = $request->message;
    $con->save();
    return redirect('/home');
}
}
