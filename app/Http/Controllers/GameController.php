<?php

namespace App\Http\Controllers;

use App\Game;
use Illuminate\Http\Request;

class GameController extends Controller
{
    public function home_page(){
        //$games= Game::select('*')->get();

        $games =Game::select('*')->orderBy('rating', 'DESC')->get();

        $best = $games[0];

        return view('pages.index',compact('best','games'));
    }

  public function welcome_home(){
        $games =Game::select('*')->orderBy('rating', 'DESC')->get();

        $best = $games[0];
        return view('welcome',compact('best','games'));
    }

    public function game_new($game_id){

        $game = Game::where('game_id',$game_id)->get();
        return view('pages.description',compact('game'));
    }

    public function game_page($game_id){

        $game = Game::where('game_id',$game_id)->get();
        //dd($game);
        return view('pages.game_description',compact('game'));
    }
}
