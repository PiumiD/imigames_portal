<?php

namespace App\Http\Controllers;

use App\Game;
use App\Lead;
use App\Leader_Board;
use App\User;
use App\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class LeadController extends Controller
{

    public function getName()
    {
        $name = DB::table('games')->pluck('game_id', 'game_name');
        return view('pages.leader_board',compact('name'));
    }

    public function getUser(Request $request)
    {

        $game_id = $request->id;
        $lead = Leader_Board::where("game_id", $game_id)->orderBy('score', 'desc')->pluck("user_id")->take(10);


      //  return $this->getUserName($lead,$game_id);

            $name=array();
            $high=array();


        $lead_col = collect($lead);
        $lead_unique = $lead_col->unique()->toArray();


            foreach ( $lead_unique as $nam) {
                $name[] = User::where("id", $nam)->pluck("user_name")->toArray();
            }

           // $high = Leader_Board::where("game_id", $game_id)->groupBy("user_id")->max("score");
             //  $high[]=DB::table('leader_board')->select('*',DB::raw("max(score) where(game_id,$game_id) group by(user_id)"))
             //->get($high);

         $high[]= DB::select("SELECT max(score) FROM leader_board where `game_id`=".$game_id." group by user_id order by score DESC");

        $length = count($high[0]);
           for($a=0;$a<$length;$a++){
               $max = "max(score)";
                $array[]=$high[0][$a]->$max;
           }
      //  dd($array);

         $gname[]=Game::where("game_id",$game_id)->pluck("game_name")->toArray();

         $des[]=Game::where("game_id",$game_id)->pluck("description")->toArray();

         $pic[]=Game::where("game_id",$game_id)->pluck("game_pic")->toArray();

        $gurl[]=Game::where("game_id",$game_id)->pluck("game_url")->toArray();

         $ratin[]=Game::where("game_id",$game_id)->pluck("rating")->toArray();

        $arr_col = collect([$name]);
        $smerged = $arr_col->merge([$array]);
        $tmerged = $smerged->merge([$gname]);
        $fmerged = $tmerged->merge([$des]);
        $gmerged=$fmerged->merge([$pic]);
        $hmerged=$gmerged->merge([$gurl]);
        $merged=$hmerged->merge([$ratin]);
        $merged->toArray();
        //dd($merged);

            return json_encode($merged);
        }
}
