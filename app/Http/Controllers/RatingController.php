<?php

namespace App\Http\Controllers;
use App\Events\commentadded;
use App\Comment;
use App\Game;
use App\User;
use App\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class RatingController extends Controller
{
    public function add_star(Request $request){
        $user_id =Auth::user()->id;

        $star= new Rating;
        $star->game_id=$request->game_id;
        $star->no_stars=$request->star;
        $star->user_id= $user_id;
        $star->save();

        $comment= new Comment();
        $comment->comment=$request->comment;
        $comment->user_id=$user_id;
        $comment->game_id=$request->game_id;
        $comment->save();

        $id =$comment-> id;
        $order = Comment::findOrFail($id);
        event(new commentadded($order));

        return $this->rating($request->game_id);


    }

    public function getfewcomment(Request $request){
        $game_id=$request->gid;
        $joinc =User::join('comments', 'comments.user_id', '=', 'users.id')->select('users.*','comments.*')->where("game_id",$game_id)->get();
        $joinr =User::join('ratings', 'ratings.user_id', '=', 'users.id')->select('users.*','ratings.*')->where("game_id",$game_id)->get();
        $array = array_merge(['commentdata'=>$joinc], ['ratedata'=>$joinr]);
        return  json_encode($array);

    }




    public function rating($game_id){

        $z=0;$u=0;
        $q=(int)$game_id;
        $rating = Rating::select('*')->get();
        $y=count($rating);
        if($y>0){
            for($x=0;$x<$y;$x++){
                if($rating[$x]->game_id == $q){
                  $z =$rating[$x]->no_stars + $z;
                  $u = ++$u;
                }
            }
            $final= $z/$u;
            Game::where('game_id',$game_id)->update(['rating'=> $final]);

        }
    }
    public function add_com(Request $request){

    }
}

