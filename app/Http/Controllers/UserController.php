<?php

namespace App\Http\Controllers;

use App\Leader_Board;
use App\User;
use App\Rating;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Requests\ProfileRequest;

class UserController extends Controller{

    public function ajax(Request $request){

        $id=$request->id;
        $gid=$request->gid;
        $name=User::where('id',$id)->get()->toArray();
        $star=Rating::where(['game_id'=>$gid,'user_id'=> $id])->orderBy('created_at', 'dsc')->pluck('no_stars')->first();
        $array = array_merge(['details'=>$name], ['rate'=>$star]);
        return  json_encode($array) ;

    }

    public function view_profile(){
        $user_id =Auth::user()->id;
        $q=0;
        $rowScore =Leader_Board::with(['user','game'])->orderBy('score', 'DESC')->where('user_id', $user_id)
            ->take(1)->get();
        if(!empty($rowScore[0])){
            //To get rank
            $list = Leader_Board::orderBy('score', 'DESC')->where('game_id', $rowScore[0]->game_id)->get();

            $x= count($list);

            for($y=0;$y<$x;$y++){
                if(($list[$y]->score == $rowScore[0]->score)&& ($list[$y]->user_id == $rowScore[0]->user_id) ){
                    $q++;
                    break;
                }else{
                    $q++;
                }
            }

            //To get played lists
            $gameList = Leader_Board::where('user_id', $user_id)->distinct()->pluck('game_id');
            for($z=0;$z<count($gameList);$z++){

                $playlists[$z] =Leader_Board::with(['user','game'])->orderBy('score', 'DESC')->where('user_id',$user_id)
                    ->where('game_id', $gameList[$z])->take(1)->get();

                foreach ($playlists[$z] as $playlist){
                    $items[] = $playlist;
                }
            }

            $users = ['profile_pic'=>$rowScore[0]->user->profile_pic,'user_id'=>$rowScore[0]->user->id,
                'user_name'=>$rowScore[0]->user->user_name,'email'=>$rowScore[0]->user->email,'password'=>$rowScore[0]->user->password,
                'score'=>$rowScore[0]->score,'rank'=>$q];
            return view('pages.profile_edit',compact('users','items'));
        }
        else{
            $new_user = User::where('id', $user_id)->get();

            $users = ['user_name'=>$new_user[0]->user_name,'email'=>$new_user[0]->email,'user_id'=>$new_user[0]->id,
                'password'=>$new_user[0]->password,'score'=>'0','rank'=>'0','profile_pic'=>$new_user[0]->profile_pic];
            $items[]= null;
            return view('pages.profile_edit',compact('users','items'));
        }
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_name' => 'required|max:15',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    public function save_profile(Requests\ProfileRequest $request){

        $user = User::find($request->user_id);
        $file = Input::file('file');
        if(!empty($file)) {
            $name = $file->getClientOriginalName();
            $file->move(public_path() . '/images/', $name);
            $user->profile_pic = ("images/" . $name);
        }
        if(!empty($request->user_name)){
            $user->user_name = $request->user_name;
        }
        if(!empty($request->email)){
            $user->email = $request->email;
        }
        if(!empty($request->password)){
            $user->password = $request->password;
        }
        $user->save();
        return redirect('/profile');
        print_r($_POST);

    }
}