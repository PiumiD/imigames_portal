<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_name' =>'required',
            'email' =>'required|email',
            'password' =>'required|min:6',
        ];
    }
    public function messages()
    {

        return [
            'user_name.required'=>'Name is required.',
            'email.required'=>'Email is required.',
            'password.required'=>'Password is required.',
            'password.min'=>'Password must be at least six characters.',
        ];
    }


}
