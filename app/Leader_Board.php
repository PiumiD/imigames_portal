<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leader_Board extends Model
{
    protected $table = 'leader_board';

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
    public function game(){
        return $this->belongsTo('App\Game','game_id','game_id');
    }
}
