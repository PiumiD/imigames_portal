<?php

namespace App\Listeners;

use App\Events\commentadded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommentListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  commentadded  $event
     * @return void
     */
    public function handle(commentadded $event)
    {
        $this->comment = $event->comment;
        $cmt=$this ->comment;
        $comments = $cmt->comment ;
    }
}
