<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],


    'facebook' => [
        'client_id' => '1824982294440840',
        'client_secret' => 'be132849ce4c414143d8110f8d6c6bd4',
        'redirect' => 'http://devstaging.arimaclanka.com/auth/facebook/callback',
],

    'twitter' => [
        'client_id' => '1e45k4dMkoOx8CPMhDXqxhR1v',
        'client_secret' => 'bu6XnIjiFy2Ph5NswMLFsWDRGE0eWbCv4LEDF35Nb7L3bJvNyL',
        'redirect' => 'http://devstaging.arimaclanka.com/auth/twitter/callback',
    ],

    'google' => [
        'client_id' => '644328966540-shon7djsu9t390pu69hi3uhpedf1skpd.apps.googleusercontent.com',
        'client_secret' => '9SmDbq7ETcCtpRgnRLd-qEz5',
        'redirect' => 'http://devstaging.arimaclanka.com/auth/google/callback',
    ],


];
