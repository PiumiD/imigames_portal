//----------------------------------responsive sizes
auto_block_size();

$(window).resize(function () {
    auto_block_size();
});

function auto_block_size(){
    var topgameele = $(".top_game_ele");
    var topgmaeeledouble = $(".top_game_ele_double");
    var block_width = topgameele.width();
    topgameele.height(block_width);
    topgmaeeledouble.height(block_width);

}
//----------------------------------responsive sizes


    //star rating
    $(".game_ele_rate").rateYo({
        starWidth: "30px",
        maxValue: 5,
        halfStar: true,
        spacing   : "5px",
        normalFill: "#fff",
        onSet: function (rating) {
            setTimeout(function () {
                disply_rate(rating);
            },100);
        },
        ratedFill: "#ffd321",
        rating : 4,
        starSvg: "<svg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 24 24' style='enable-background:new 0 0 24 24;' xml:space='preserve'>"+
        "<path d='M12.6,1.9l3,6.3l6.9,1c0.6,0.1,0.8,0.8,0.4,1.2l-5,4.8l1.2,6.9c0.1,0.6-0.5,1-1,0.7L12,19.4l-6.1,3.3 c-0.5,0.3-1.1-0.2-1-0.7l1.2-6.9l-5-4.8C0.6,9.9,0.9,9.2,1.4,9.1l6.9-1l3-6.3C11.6,1.4,12.4,1.4,12.6,1.9z'/>"+
        "</svg>"
    });

    $(".game_ele_rate").click(function () {
        var $rateYo = $(".rate_on_modal").rateYo();
        $rateYo.rateYo("destroy");
    });

    $(".game_ele_rate_home").rateYo({
        starWidth: "30px",
        maxValue: 5,
        halfStar: true,
        readOnly: true,
        spacing   : "5px",
        normalFill: "#fff",
        ratedFill: "#ffd321",
        rating : 3.5,
        starSvg: "<svg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 24 24' style='enable-background:new 0 0 24 24;' xml:space='preserve'>"+
        "<path d='M12.6,1.9l3,6.3l6.9,1c0.6,0.1,0.8,0.8,0.4,1.2l-5,4.8l1.2,6.9c0.1,0.6-0.5,1-1,0.7L12,19.4l-6.1,3.3 c-0.5,0.3-1.1-0.2-1-0.7l1.2-6.9l-5-4.8C0.6,9.9,0.9,9.2,1.4,9.1l6.9-1l3-6.3C11.6,1.4,12.4,1.4,12.6,1.9z'/>"+
        "</svg>"
    });

    /*$(".game_ele_rate_block").rateYo({
        starWidth: "20px",
        maxValue: 5,
        halfStar: true,
        readOnly: true,
        spacing   : "5px",
        normalFill: "#fff",
        ratedFill: "#ffd321",
        rating : 3.5,
        starSvg: "<svg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 24 24' style='enable-background:new 0 0 24 24;' xml:space='preserve'>"+
        "<path d='M12.6,1.9l3,6.3l6.9,1c0.6,0.1,0.8,0.8,0.4,1.2l-5,4.8l1.2,6.9c0.1,0.6-0.5,1-1,0.7L12,19.4l-6.1,3.3 c-0.5,0.3-1.1-0.2-1-0.7l1.2-6.9l-5-4.8C0.6,9.9,0.9,9.2,1.4,9.1l6.9-1l3-6.3C11.6,1.4,12.4,1.4,12.6,1.9z'/>"+
        "</svg>"
    });*/

    $(".game_ele_rate_profile").rateYo({
        starWidth: "20px",
        maxValue: 5,
        halfStar: true,
        readOnly: true,
        spacing   : "5px",
        normalFill: "#fff",
        ratedFill: "#ffd321",
        rating : 3.5,
        starSvg: "<svg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 24 24' style='enable-background:new 0 0 24 24;' xml:space='preserve'>"+
        "<path d='M12.6,1.9l3,6.3l6.9,1c0.6,0.1,0.8,0.8,0.4,1.2l-5,4.8l1.2,6.9c0.1,0.6-0.5,1-1,0.7L12,19.4l-6.1,3.3 c-0.5,0.3-1.1-0.2-1-0.7l1.2-6.9l-5-4.8C0.6,9.9,0.9,9.2,1.4,9.1l6.9-1l3-6.3C11.6,1.4,12.4,1.4,12.6,1.9z'/>"+
        "</svg>"
    });

    $(".star_rating_comment").rateYo({
        starWidth: "10px",
        maxValue: 5,
        halfStar: true,
        readOnly: true,
        spacing   : "5px",
        normalFill: "#999",
        ratedFill: "#ffd321",
        rating : 3.5,
        starSvg: "<svg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 24 24' style='enable-background:new 0 0 24 24;' xml:space='preserve'>"+
        "<path d='M12.6,1.9l3,6.3l6.9,1c0.6,0.1,0.8,0.8,0.4,1.2l-5,4.8l1.2,6.9c0.1,0.6-0.5,1-1,0.7L12,19.4l-6.1,3.3 c-0.5,0.3-1.1-0.2-1-0.7l1.2-6.9l-5-4.8C0.6,9.9,0.9,9.2,1.4,9.1l6.9-1l3-6.3C11.6,1.4,12.4,1.4,12.6,1.9z'/>"+
        "</svg>"
    });

    function disply_rate(user_rating) {
        $("#compose_comment").modal();
        $(".rating_value").text(user_rating);
        $(".rate_on_modal").rateYo({
            starWidth: "20px",
            maxValue: 5,
            halfStar: true,
            readOnly: true,
            spacing   : "5px",
            normalFill: "#999",
            ratedFill: "#ffd321",
            rating : user_rating,
            starSvg: "<svg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 24 24' style='enable-background:new 0 0 24 24;' xml:space='preserve'>"+
            "<path d='M12.6,1.9l3,6.3l6.9,1c0.6,0.1,0.8,0.8,0.4,1.2l-5,4.8l1.2,6.9c0.1,0.6-0.5,1-1,0.7L12,19.4l-6.1,3.3 c-0.5,0.3-1.1-0.2-1-0.7l1.2-6.9l-5-4.8C0.6,9.9,0.9,9.2,1.4,9.1l6.9-1l3-6.3C11.6,1.4,12.4,1.4,12.6,1.9z'/>"+
            "</svg>"
        });
    }




$(document).ready(function () {
    set_profile_info_w();
    $(window).resize(function () {
        var doc_width = $(window).width();
        remove_uw_xs_class(doc_width);
        if($(window).width()>769){
            set_profile_info_w();
        }
    });

    $(".navi_links ul li").click(function(){
        if($(window).width()<769){
            $('.navbar-toggle').click();
        }
    });
    
    function remove_uw_xs_class(w_width) {
        if(w_width>768){

            if($(".imi_logo").is(".logo_small")){
                $(".imi_logo").removeClass("logo_small");
                $(".navbar-toggle").trigger( "click" );
                $('.navbar-toggle').click();
            }
            if($(".navi_styles>div").is(".trans_black_bg")){
                $(".navi_styles>div").removeClass("trans_black_bg");
            }
        }
    }

    function set_profile_info_w() {
        var row_width = $(".game_detail_row_r").outerWidth();
        var pro_pic = $(".profile_pic").outerWidth();
        //var pro_info_pad = $(".profie_info_warapper").css("padding-left");
        //alert(win_width+" and "+pro_info_pad+" and "+pro_pic);
        $(".profile_info").width(row_width-159);
    }
    
    $(".mob_menu_tog").click(function () {
        $(".imi_logo").toggleClass("logo_small");
        $(".navi_styles>div").toggleClass("trans_black_bg");
    });

    //home block FX
   /* var hover_count = 0;
    $(".block_both_sides").mouseenter(function () {
        var element_bkside = $(this).find(".block_back_side");
        if(hover_count%2==0){
            if(element_bkside.is(".rotate_X_axis")){
                element_bkside.removeClass("rotate_X_axis");
            }
            else if(!element_bkside.is(".rotate_Y_axis")){
                element_bkside.addClass("rotate_Y_axis");
            }
            $(this).find(".block_front_side").addClass("rotateY_180");
            $(this).find(".block_back_side").addClass("rotateY_360");
            hover_count++;
        }
        else{
            if(element_bkside.is(".rotate_Y_axis")){
                element_bkside.removeClass("rotate_Y_axis");
            }
            else if(!element_bkside.is(".rotate_X_axis")){
                element_bkside.addClass("rotate_X_axis");
            }
            $(this).find(".block_front_side").addClass("rotateX_180");
            $(this).find(".block_back_side").addClass("rotateX_360");
            hover_count++;
        }
    });

    $(".block_both_sides").mouseleave(function () {
        $(this).find(".block_front_side").removeClass("rotateY_180 rotateX_180");
        $(this).find(".block_back_side").removeClass("rotateY_360 rotateX_360");
    });*/
    //end of home block FX

    //profile edit UX
    $(".dr_pencil_icon").click(function(){
        $("#pro_info_save").slideDown(500);
        $(".detail_un input").attr("readonly", "readonly");
        $(".detail_un input").removeClass("blink_anim");
        var catch_ele = $(this).parent("div").siblings("div").children(".detail_un").children("input");
        catch_ele.removeAttr("readonly");
        catch_ele.focus().addClass("blink_anim");

    });

    $("#pro_info_save").click(function () {
        $(this).slideUp();
        $(".detail_un input").attr("readonly", "readonly");
        $(".detail_un input").removeClass("blink_anim");
    });

    $(".edit_tog").click(function () {
        $("#pro_pic_uploader").click();
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                var filename = input.value;
                var lastIndex = filename.lastIndexOf("\\");
                if (lastIndex >= 0) {
                    filename = filename.substring(lastIndex + 1);
                }

                $('.profile_pic').css('background-image', 'url('+e.target.result+')');
                $("#pro_info_save").slideDown(500);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#pro_pic_uploader").change(function(){
        readURL(this);

    });
    //end of profile edit UX
    
    //profile palyed game list anim
    $(".played_game_list").click(function () {
        $(".played_game_list").removeClass("profile_game_list_selected");
        $(".played_game_list").addClass("profile_game_list_item");
        $(this).addClass("profile_game_list_selected");
        $(this).removeClass("profile_game_list_item");
    });
    /*$(".played_game_list").mouseleave(function () {
        $(this).removeClass("profile_game_list_selected");
        $(this).addClass("profile_game_list_item");
    });*/

    //scroll to comment section
    $("#place_comment").click(function() {
        $('html, body').animate({
            scrollTop: $(".comment_sec").offset().top
        },2000,"swing");
    });

    //set rating stars
//0.0
    $(".star_rating_0_0").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_0_0").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_0_0").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_0_0").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_0_0").append('<i class="fa fa-star-o" aria-hidden="true"></i>');

//0.5
    $(".star_rating_0_5").append('<i class="fa fa-star-half-o" aria-hidden="true"></i>');
    $(".star_rating_0_5").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_0_5").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_0_5").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_0_5").append('<i class="fa fa-star-o" aria-hidden="true"></i>');

//1.0
    $(".star_rating_1_0").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_1_0").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_1_0").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_1_0").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_1_0").append('<i class="fa fa-star-o" aria-hidden="true"></i>');

//1.5
    $(".star_rating_1_5").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_1_5").append('<i class="fa fa-star-half-o" aria-hidden="true"></i>');
    $(".star_rating_1_5").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_1_5").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_1_5").append('<i class="fa fa-star-o" aria-hidden="true"></i>');

//2.0
    $(".star_rating_2_0").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_2_0").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_2_0").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_2_0").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_2_0").append('<i class="fa fa-star-o" aria-hidden="true"></i>');

//2.5
    $(".star_rating_2_5").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_2_5").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_2_5").append('<i class="fa fa-star-half-o" aria-hidden="true"></i>');
    $(".star_rating_2_5").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_2_5").append('<i class="fa fa-star-o" aria-hidden="true"></i>');

//3.0
    $(".star_rating_3_0").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_3_0").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_3_0").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_3_0").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
    $(".star_rating_3_0").append('<i class="fa fa-star-o" aria-hidden="true"></i>');

//3.5
    $(".star_rating_3_5").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_3_5").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_3_5").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_3_5").append('<i class="fa fa-star-half-o" aria-hidden="true"></i>');
    $(".star_rating_3_5").append('<i class="fa fa-star-o" aria-hidden="true"></i>');

//4.0
    $(".star_rating_4_0").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_4_0").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_4_0").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_4_0").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_4_0").append('<i class="fa fa-star-o" aria-hidden="true"></i>');

//4.5
    $(".star_rating_4_5").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_4_5").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_4_5").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_4_5").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_4_5").append('<i class="fa fa-star-half-o" aria-hidden="true"></i>');

//5.0
    $(".star_rating_5_0").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_5_0").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_5_0").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_5_0").append('<i class="fa fa-star" aria-hidden="true"></i>');
    $(".star_rating_5_0").append('<i class="fa fa-star" aria-hidden="true"></i>');

//end of set rating stars

});

