<div class="row footer">
    <div class="col-sm-3 company_text">
        <p>imi games @ 2016</p>
    </div>
    <div class="col-sm-6">
        <ul class="footer_navi_list">
            <li>© Arimac Lanka PLC. All Rights Reserved.</li>
            <li><a href="#" class="footer_navi">Terms & Conditions</a></li>
            <li>|</li>
            <li><a href="#" class="footer_navi">Privacy Policy</a></li>
            <li>|</li>
            <li><a href="#" class="footer_navi">Sitemap</a></li>
        </ul>
    </div>
    <div class="col-sm-3">
        <ul class="social_icon_list">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
            <li><a href="#" class="social_icons"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="#" class="social_icons"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="#" class="social_icons"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
            <li><a href="#" class="social_icons"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        </ul>
    </div>
</div>
<script src="{{ URL::asset('js/imi_custom.js')}}"></script>
</body>
</html>