<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>iMi GAMES</title>
    <link href="{{ URL::asset('images/favicon.png')}}" rel="icon" type="image/png" sizes="32x32">
    <link href="{{ URL::asset('css/bootstrap.min.css')}}" rel="stylesheet" >
    <link href="{{ URL::asset('css/font-awesome.min.css')}}" rel="stylesheet" >
    <link href="{{ URL::asset('css/jquery.rateyo.min.css')}}" rel="stylesheet" >
    <link href="{{ URL::asset('css/icon-font.min.css')}}" rel="stylesheet" >
    <link href="{{ URL::asset('css/imi_styles.css')}}" rel="stylesheet" >

    <script src="{{ URL::asset('js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js')}}"></script>
    <script src="{{ URL::asset('js/jquery.rateyo.min.js')}}"></script>

    {{----}}
    {{--<link rel="icon" href="images/favicon.png" type="image/png" sizes="32x32">--}}
    {{--<link href="css/bootstrap.min.css" rel="stylesheet">--}}
    {{--<link href="css/font-awesome.min.css" rel="stylesheet">--}}
    {{--<link href="css/jquery.rateyo.min.css" rel="stylesheet">--}}
    {{--<link href="css/icon-font.min.css" rel="stylesheet">--}}
    {{--<link href="css/imi_styles.css" rel="stylesheet">--}}
    {{--<script src="js/jquery-3.1.1.min.js"></script>--}}
    {{--<script src="js/bootstrap.min.js"></script>--}}
    {{--<script src="js/jquery.rateyo.min.js"></script>--}}
    {{--<script src="js/imi_custom.js"></script>--}}
</head>
<body>