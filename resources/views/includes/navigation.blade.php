<nav class="navbar navbar-default navi_styles">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed mob_menu_tog" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand imi_logo" href="#"><img src="images/imi_logo.png"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navi_links" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('/first') }}">Home</a></li>
                <li class="active_link"><a href="{{ url('/profile') }}">Profile</a></li>
                <li><a href="{{ url('/leader_board') }}">Leaderboard</a></li>
                <li><a href="#" data-toggle="modal" data-target="#contact">Contact</a></li>


                <li>
                    <a href="{{ url('/logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>



                {{--<li><a href="{{url('/logout')}}"  >Log Out</a></li>--}}
                {{--<li><a href="#" data-toggle="modal" data-target="#signup">Sign Up</a></li>--}}
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>