
<!-- Sing in modal-->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content login_signup_modals" style="background-image: url('{{URL::asset('images/popup_modal_bg.jpg')}}');">
            <button type="button" class="modal_close" data-dismiss="modal" aria-label="Close"><span class="lnr lnr-cross"></span></button>
            <h1 class="text-center modal_title">Welcome Back</h1>
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1 input_cont">
                    <form class="form-horizontal" role="form" method="POST" action="{{url('/login')}} ">
                        {{ csrf_field() }}

                        <div class="form-group input_styles {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-sm-3 control-label">Email</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group input_styles{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="password"  class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>
                        <div class="form-group input_styles">
                            <div class="col-md-6 col-md-offset-1">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group input_styles">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-default col-xs-12">Sign In</button>
                            </div>
                        </div>

                    </form>

                    <div class="form-group social_login input_styles">
                        <div class="col-sm-12">
                            <a class="facebook_login" href="{{url('/auth/facebook')}}"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a class="google_login" href="{{url('/auth/google')}}"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                            <a class="twitter_login" href="{{url('/auth/twitter')}}"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- end Sing in modal-->

<!-- Sing up modal-->
<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content login_signup_modals" style="background-image: url('{{URL::asset('images/popup_modal_bg.jpg')}}');">
            <button type="button" class="modal_close" data-dismiss="modal" aria-label="Close"><span class="lnr lnr-cross"></span></button>
            <h1 class="text-center modal_title">Wanna Play?</h1>
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1 input_cont">
                    <form class="form-horizontal"role="form" method="POST" action=" {{url('/register')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group input_styles{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-sm-3 control-label">Username</label>
                            <div class="col-sm-9">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus >

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>
                        <div class="form-group input_styles{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-sm-3 control-label">Email</label>

                            <div class="col-sm-9">
                                <input id="email" type="email" class="form-control"  name="email" value="{{ old('email') }}" required >

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>
                        <div class="form-group input_styles{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9">
                                <input id="password" type="password" class="form-control" name="password" required >

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>
                        <div class="form-group input_styles">
                            <label for="password-confirm" class="col-md-3 control-label">Confirm Password</label>

                            <div class="col-md-9">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pf') ? ' has-error' : '' }}">
                            <label for="profile pic" class="col-md-3">Select Your Profile Picture</label>

                            <div class="col-md-9">
                                <input id="pic" type="file"  name="pc" >
                            </div>
                        </div>

                        <div class="form-group input_styles">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-default col-xs-12">Sign Up</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end Sing up modal-->

<!-- Contact modal-->
<div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content login_signup_modals" style="background-image: url('{{URL::asset('images/popup_modal_bg.jpg')}}');">
            <button type="button" class="modal_close" data-dismiss="modal" aria-label="Close"><span class="lnr lnr-cross"></span></button>
            <h1 class="text-center modal_title">Send a Message</h1>
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1 input_cont">
                    <form class="form-horizontal" action="{{url('/contact')}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group input_styles">
                            <label for="name" class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="name" id="name" >
                            </div>
                        </div>
                        <div class="form-group input_styles">
                            <label for="email" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" name="email" id="email">
                            </div>
                        </div>
                        <div class="form-group input_styles">
                            <label for="tel_no" class="col-sm-3 control-label">Mobile</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="tel_no" id="tel_no" >
                            </div>
                        </div>
                        <div class="form-group input_styles">
                            <label for="message" class="col-sm-3 control-label">Message</label>
                            <div class="col-sm-9">
                                <textarea type="text" class="form-control" rows="4" name="message" id="message" ></textarea>
                            </div>
                        </div>
                        <div class="form-group input_styles">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-default col-xs-12">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Success message modal-->
<div class="modal fade" id="success_msg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content login_signup_modals">
            <button type="button" class="modal_close" data-dismiss="modal" aria-label="Close"><span class="lnr lnr-cross"></span></button>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="glyphicon glyphicon-ok success_icon"></div>
                    <h2 class="text-center msg_modal_title">Message Title</h2>
                    <p class="msg_modal_cont text-center col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-xs-12">Lorem ipsum dolor sit amet,
                        ultrices mauris lacinia pede elementum erat in,
                        fusce lorem posuere eleifend, est nunc orci,</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-xs-12 input_cont_msg">
                    <div class="form-horizontal">

                        <div class="form-group input_styles">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-default col-xs-12" id="place_comment">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of success modal-->
<!-- error message modal-->
<div class="modal fade" id="error_msg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content login_signup_modals">
            <button type="button" class="modal_close" data-dismiss="modal" aria-label="Close"><span class="lnr lnr-cross"></span></button>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="glyphicon glyphicon-remove error_icon"></div>
                    <h2 class="text-center msg_modal_title">Message Title</h2>
                    <p class="msg_modal_cont text-center col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-xs-12">Lorem ipsum dolor sit amet,
                        ultrices mauris lacinia pede elementum erat in,
                        fusce lorem posuere eleifend, est nunc orci,</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-xs-12 input_cont_msg">
                    <div class="form-horizontal">

                        <div class="form-group input_styles">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-default col-xs-12" id="place_comment">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end error modal-->