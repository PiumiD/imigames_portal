<!-- add comment modal-->

    <head>
        <script>
            $(document).ready(function(){
                $('#submit').click(function(){

                    var x = $('#star').text();
                    var y = $('textarea#message').val();
                    var game ={{$game[0]->game_id}}
                    $.ajax( {
                        type : 'get',
                        url  : '{{url('/star')}}',
                        data : { star : x,game_id : game,comment : y},

                        dataType: 'json',
                        success: function(data){
                            alert(data);


                        }
                    } );

                });
            });


        </script>
    </head>

<div class="modal fade" id="compose_comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content login_signup_modals">
            <button type="button" class="modal_close" data-dismiss="modal" aria-label="Close"><span class="lnr lnr-cross"></span></button>
            <h1 class="text-center modal_title">Compose a Comment</h1>
            <div class="rate_info">
                <div class="rate_on_modal"></div>
                <div class="rating_value" name="star" id="star"></div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-xs-12 input_cont">
                    <div class="form-horizontal">
                        <div class="form-group input_styles">
                            <label for="message" class="col-sm-3 col-xs-12 control-label">Comment</label>
                            <div class="col-sm-9 col-xs-12">
                                <textarea type="text" class="form-control" rows="4" name="comment" id="message" ></textarea>
                            </div>
                        </div>
                        <div class="form-group input_styles">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-default col-xs-12"   id="submit" data-dismiss="modal">Add Comment </button>
                                {{--<button type="submit" class="btn btn-default col-xs-12" id="submit" >Place comment</button>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end add comment modal-->