<!DOCTYPE html>
<html>
<head>

</head>
<body>

@include('includes.header')
@include('includes.navi')
@include('includes.popup_modals')

@yield('content')

@include('includes.footer')

</body>
</html>