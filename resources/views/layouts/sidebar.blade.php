<!DOCTYPE html>
<html>
<head>

</head>
<body>

@include('includes.header')
@include('includes.navigation')
@include('includes.popup_modals')

@yield('content')

@include('includes.footer')

</body>
</html>