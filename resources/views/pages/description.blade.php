<html>
<head>
    <meta name="csrf-token" content="<?php echo csrf_token() ?>">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//js.pusher.com/4.0/pusher.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

</head>
<body>

<script>
    Pusher.logToConsole = true;

    var pusher = new Pusher('19dd501dd82d5c9bf24c', {
        authTransport: 'jsonp',
        authEndpoint: 'http://myserver.com/pusher_jsonp_auth'
    });

    var channel = pusher.subscribe('comment');
    channel.bind('App\\Events\\commentadded',function (data) {
            //  alert(data.comment.user_id +' commented at'+ data.comment.game_id+' th game as..... ' + data.comment.comment);

            //  var name =data.comment.name;
            //  var comment=data.comment.comment;

            //  alert(name +' commented as..... ' + comment);

            // document.write(name)
            // document.write(comment)

            // document.write(name +' commented as..... ' +comment);

            var game_id =data.comment.game_id;
            var user_id =data.comment.user_id;
            var comment=data.comment.comment;
            var id=data.comment.id;

            $.ajax({
                url:'{{ url('getname') }}',
                type: "get",
                data: { id : user_id ,gid :game_id },
                success: function (array) {
                    ob = $.parseJSON(array);

                    // data =$.parseJSON(ob.details);
                    console.log(array);
                    data=ob.details;

                    //  console.log(data[0].user_name);
                    name=data[0].user_name
                    console.log(name); path=data[0].profile_pic; console.log(path);


                    b1 ="\{\{ URL\:\:to\(\'\/\'\)\}\}\/" ;
                    b2 ="{{ URL::to('/') }}/";
                    // basepath="url\(\'"+ path +"\'\)";
                    // alert(basepath);
                    //  alert("Game id: "+ game_id +" rate : "+ ob.rate + " name : " + name  + " comment as " + comment );

                    var html =' <div class="col-md-6 col-sm-6 set_comment_pad"> <div class="media"> <div class="media-left"> <a href="#"><div class="" ></div> <img class="media-object set_img_size" src='+b2+path+'  alt='+path+' /></a> </div> <div class="media-body"> <h4 class="media-heading" id="md">'+ name +'  </h4> <p class="comment_description"> '+comment+' </p> <div class="row"> <div class="col-sm-6"> <div class="star_rating_comment"></div> '+ob.rate+'</div> <p class="col-sm-6 text-right comment_date">01/25/2017</p> </div> </div> </div> </div>';

                    var ccomment=$("#comments");
                    ccomment.prepend(html);

                    // $('#picoutput').html(ob);
                }

            });

        }
    )

</script>
@extends('layouts.default1')
@section('content')


    <form method="get" enctype="multipart/form-data">
        <div class="row">
            <div class="col-sm-12 header_full_length" style="background-image: url('{{$game[0]->game_pic}}');">
                <div class="bg_overlay bg_shdw_static">
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1 game_info">
                            <h1 class="game_title">{{$game[0]->game_name}}</h1>
                            <p class="short_descri">{{$game[0]->description}} </p>
                            <div class="game_ele_rate" id="xx"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 game_descri_wrapper">
                <div class="row hidden-xs game_descri_single">
                    <div class="col-sm-5 game_descri_img" style="background-image: url('images/game_descri_img_2.jpg');">

                    </div>
                    <div class="col-sm-7 game_descri_txt">
                        <h2>Damn The Castle</h2>
                        <p>Lorem ipsum dolor sit amet, mauris dapibus sit eu, wisi ligula.
                            Neque et per, sodales fusce condimentum tortor lacus felis,
                            sed suspendisse lobortis integer.
                        </p>
                    </div>
                </div>
                <div class="row visible-xs game_descri_single">
                    <div class="col-sm-7 game_descri_txt">
                        <h2>Damn The Castle</h2>
                        <p>Lorem ipsum dolor sit amet, mauris dapibus sit eu, wisi ligula.
                            Neque et per, sodales fusce condimentum tortor lacus felis,
                            sed suspendisse lobortis integer.
                        </p>
                    </div>
                    <div class="col-sm-5 game_descri_img" style="background-image: url('images/game_descri_img_2.jpg');">

                    </div>
                </div>
                <div class="row game_descri_single">
                    <div class="col-sm-7 game_descri_txt">
                        <h2>Damn The Castle</h2>
                        <p>Lorem ipsum dolor sit amet, mauris dapibus sit eu, wisi ligula.
                            Neque et per, sodales fusce condimentum tortor lacus felis,
                            sed suspendisse lobortis integer.
                        </p>
                    </div>
                    <div class="col-sm-5 game_descri_img" style="background-image: url('images/game_descri_img_3.jpg');">

                    </div>
                </div>
            </div>
        </div>
        <div class="row comment_sec">
            <div class="col-md-12">
                <div class="row" id="comments">

                </div>
            </div>
        </div>

    </form>
@endsection
</body>
</html>