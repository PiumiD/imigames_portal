<html>
<head>
    <meta name="csrf-token" content="<?php echo csrf_token() ?>">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//js.pusher.com/4.0/pusher.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

</head>
<body>
<script>
    var contents = "{!! addcslashes($game[0], '"') !!}";
    game = $.parseJSON(contents);
    thisgameid=game.game_id
    console.log(thisgameid);
    $(document).ready(function() {
        $.ajax({
            url: '{{ url('getfewcomment') }}',
            type: "get",
            data: {gid: thisgameid},
            success: function (response) {
                cmt = $.parseJSON(response);
                console.log(response);
                console.log(cmt.commentdata);
                console.log(cmt.commentdata[0].comment);
                console.log(cmt.ratedata[0].no_stars);
                for(var i=0;i<cmt.commentdata.length;i++)
                {


                    console.log(cmt.commentdata[i].user_name);console.log(cmt.commentdata[i].profile_pic);
                    console.log(cmt.commentdata[i].comment);console.log(cmt.ratedata[i].no_stars);
                    b2 ="{{ URL::to('/') }}/";
                    var numberd=i;
                    var ss=cmt.ratedata[i].no_stars;
                    var html =' <div class="col-md-6 col-sm-6 set_comment_pad"> <div class="media"> <div class="media-left"> <a href="#"><div class="" ></div><img class="media-object set_img_size" src='+b2+cmt.commentdata[i].profile_pic+'  alt='+cmt.commentdata[i].profile_pic+' /> </a> </div> <div class="media-body"> <h4 class="media-heading" id="md">'+cmt.commentdata[i].user_name+'  </h4> <p class="comment_description"> '+cmt.commentdata[i].comment+' </p> <div class="row"> <div class="col-sm-6"> <div class="star_rating" rate="'+cmt.ratedata[i].no_stars+'" id="'+numberd+'"></div></div> <p class="col-sm-6 text-right comment_date">'+cmt.commentdata[i].created_at+'</p> </div> </div> </div> </div>';
                    var cccomment=$("#comments");
                    cccomment.prepend(html);
                    console.log("brfore"+ss);
                    console.log("end");
                    console.log("last end");

                }
                $('.star_rating').each(function (i, obj) {
                    console.log("inside");
                    var rate = ss;
                    console.log(rate);
                    var id = $(this).attr('id');
                    var rate = $(this).attr('rate');
                    $("#" + id).rateYo({
                        rating: rate,
                        starWidth: "10px",
                        maxValue: 5,
                        halfStar: true,
                        readOnly: true,
                        spacing: "5px",
                        normalFill: "#999",
                        ratedFill: "#ffd321",
                    });

                });


                console.log(response.user_id);
                cuid = response.user_id;
                console.log(cuid);


            }

        });

    });



    Pusher.logToConsole = true;

    var pusher = new Pusher('19dd501dd82d5c9bf24c', {
        authTransport: 'jsonp',
        authEndpoint: 'http://myserver.com/pusher_jsonp_auth'
    });

    var channel = pusher.subscribe('comment');
    channel.bind('App\\Events\\commentadded',function (data) {

            var game_id =data.comment.game_id;
            var user_id =data.comment.user_id;
            var comment=data.comment.comment;
            var id=data.comment.id;
            if (comment.game_id =thisgameid) {

                $.ajax({
                    url:'{{ url('getname') }}',
                    type: "get",
                    data: { id : user_id ,gid :game_id },
                    success: function (array) {
                        ob = $.parseJSON(array);
                        console.log(array);
                        data=ob.details;
                        name=data[0].user_name
                        console.log(name); path=data[0].profile_pic; console.log(path);console.log(ob.rate);
                        starrate=ob.rate;
                        b2 ="{{ URL::to('/') }}/";
                        var number=$(".star").length;

                        $(function (){
                            rate =starrate;
                            $("#rateYo"+number).rateYo({
                                rating:rate,
                                starWidth: "10px",
                                maxValue: 5,
                                halfStar: true,
                                readOnly: true,
                                spacing   : "5px",
                                normalFill: "#999",
                                ratedFill: "#ffd321",
                            });
                        });
                        var html = '<div class="col-md-6 col-sm-6 set_comment_pad"><div class="media"> <div class="media-left"> <a href="#"> <img class="media-object set_img_size" src='+b2+path+' alt='+path+'> </a> </div> <div class="media-body"> <h4 class="media-heading">'+ name +'</h4> <p class="comment_description">'+comment+'</p> <div class="row"> <div class="col-sm-6"> <div class="star" id="rateYo'+number+'"></div> </div> <p class="col-sm-6 text-right comment_date">Now</p> </div> </div> </div> </div>'
                        var ccomment=$("#comments");
                        ccomment.prepend(html);


                    }

                });


            }

        }
    )

</script>

<script>
    $(document).ready(function() {
    });
</script>

@extends('layouts.sidebar')
@extends('includes.rating_popup')
@section('content')

    <form method="get" enctype="multipart/form-data">
        <div class="row">
            <div class="col-sm-12 header_full_length" style="background-image: url('{{$game[0]->game_pic}}');">
                <div class="bg_overlay bg_shdw_static">
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1 game_info">
                            <h1 class="game_title">{{$game[0]->game_name}}</h1>
                            <p class="short_descri">{{$game[0]->description}} </p>
                            <div class="game_ele_rate"  ></div>
                            <div  id="str"></div>
                            <a href="{{url($game[0]->game_url)}}" class="header_play_btn">Play Game</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 game_descri_wrapper">
                <div class="row hidden-xs game_descri_single">
                    <div class="col-sm-5 game_descri_img" style="background-image: url('images/game_descri_img_2.jpg');">

                    </div>
                    <div class="col-sm-7 game_descri_txt">
                        <h2>Damn The Castle</h2>
                        <p>Lorem ipsum dolor sit amet, mauris dapibus sit eu, wisi ligula.
                            Neque et per, sodales fusce condimentum tortor lacus felis,
                            sed suspendisse lobortis integer.
                        </p>
                    </div>
                </div>
                <div class="row visible-xs game_descri_single">
                    <div class="col-sm-7 game_descri_txt">
                        <h2>Damn The Castle</h2>
                        <p>Lorem ipsum dolor sit amet, mauris dapibus sit eu, wisi ligula.
                            Neque et per, sodales fusce condimentum tortor lacus felis,
                            sed suspendisse lobortis integer.
                        </p>
                    </div>
                    <div class="col-sm-5 game_descri_img" style="background-image: url('images/game_descri_img_2.jpg');">

                    </div>
                </div>
                <div class="row game_descri_single">
                    <div class="col-sm-7 game_descri_txt">
                        <h2>Damn The Castle</h2>
                        <p>Lorem ipsum dolor sit amet, mauris dapibus sit eu, wisi ligula.
                            Neque et per, sodales fusce condimentum tortor lacus felis,
                            sed suspendisse lobortis integer.
                        </p>
                    </div>
                    <div class="col-sm-5 game_descri_img" style="background-image: url('images/game_descri_img_3.jpg');">

                    </div>
                </div>
            </div>
        </div>
        <div class="row comment_sec">
            <div class="col-md-12">
                <h2 class="comment_sec_title">Comments</h2>
                <div class="row" id="comments">


                </div>
            </div>
        </div>

</form>
@endsection
</body>
</html>