@if(\Illuminate\Support\Facades\Auth::guest())
    {{dd('Please login')}}
    @endif
<html>
<body>

<script src="js/jquery-3.1.1.min.js"></script>

<script type="text/javascript">

    $(document).ready(function(){
        changeFunc();
    });

        function changeFunc() {
        var selectBox = document.getElementById("selectBox");
        var selectedValue = selectBox.options[selectBox.selectedIndex].value;

        $.ajax({
            method: "GET",
            url:'{{ url('uid') }}',
            data: {id: selectedValue},
            dataType: 'json',
            success:function(merged){
                //console.log(data);
                //var x= JSON.parse(data);
               console.log(merged,0);

                var htmlnu="";
                for(var n=4;n<=merged[0].length;n++){
                    htmlnu+=[n];
                }
                $("#num").html(htmlnu);

                var html1="";
                for(var a=0;a<1;a++){
                    html1+=merged[0][a];
                }
                $("#user_name1").html(html1);

                var html2="";
                for(var b=1;b<2;b++){
                    html2+=merged[0][b];
                }
                $("#user_name2").html(html2);

                var html3="";
                for(var c=2;c<3;c++){
                    html3+=merged[0][c];
                }
                $("#user_name3").html(html3);

                var htmlu="";
                for(var d=3;d<merged[0].length;d++){
                    htmlu+=merged[0][d];
                }
                $("#user_name").html(htmlu);

                var htmlp1="";
                for(var e=0;e<1;e++){
                    htmlp1+=merged[1][e];
                }
                $("#point1").html(htmlp1);

                var htmlp2="";
                for(var f=1;f<2;f++){
                    htmlp2+=merged[1][f];
                }
                $("#point2").html(htmlp2);

                var htmlp3="";
                for(var g=2;g<3;g++){
                    htmlp3+=merged[1][g];
                }
                $("#point3").html(htmlp3);

                var htmlp="";
                for(var h=3;h<merged[1].length;h++){
                    htmlp+=merged[1][h];
                }
                $("#point").html(htmlp);

                var htmlText="";
                    htmlText+=merged[2];
                    $("#gnam").html(htmlText);

                var htmlTex="";
                    htmlTex+=merged[3];
                    $("#descrip").html(htmlTex);

               var htmlPic="";
              htmlPic+=merged[4];
                $('#pict').css('background-image', 'url(' + htmlPic + ')');

                var htmlUrl="";
                htmlUrl+=merged[5];
                $("#ga_url").attr("onclick","window.location.href='"+htmlUrl+"'");

                var htmlRate="";
                    htmlRate+=merged[6];

                    $('#rate').rateYo({
                        starWidth: "30px",
                        maxValue: 5,
                        onSet:null,
                        rating:htmlRate,
                        halfStar: true,
                        readOnly: true,
                        spacing: "5px",
                        normalFill: "#dfdfdf",
                        ratedFill: "#ffde00",
                        starSvg: "<svg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 24 24' style='enable-background:new 0 0 24 24;' xml:space='preserve'>"+
                        "<path d='M12.6,1.9l3,6.3l6.9,1c0.6,0.1,0.8,0.8,0.4,1.2l-5,4.8l1.2,6.9c0.1,0.6-0.5,1-1,0.7L12,19.4l-6.1,3.3 c-0.5,0.3-1.1-0.2-1-0.7l1.2-6.9l-5-4.8C0.6,9.9,0.9,9.2,1.4,9.1l6.9-1l3-6.3C11.6,1.4,12.4,1.4,12.6,1.9z'/>"+
                        "</svg>"
                });

            }
        });

    }

</script>
@extends('layouts.sidebar')
@section('content')

    <div class="row">
        <div class="col-sm-12 header_full_length" id="pict">
            <div class="bg_overlay bg_shdw_static">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 game_info">
                        <h1 class="game_title" id="gnam"></h1>
                        <p class="short_descri" id="descrip"></p>
                        <div  id="rate" value=""></div>
                        <button class="header_play_btn" id="ga_url">Play Game</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 leader_board_wrapper" style="background-image: url('images/leader_board_bg.jpg');">
            <div class="leader_board_select">
                <form action="{{url('/leader_board')}}" method="post">
                    {{ csrf_field() }}
                    <select name="game_id" id="selectBox" onchange="changeFunc();">
                        @foreach($name as $key=>$sel)
                            <option value="{{$sel}}">{{$key}}</option>
                        @endforeach
                    </select>
                </form>

            <div class="row first_leader">
                <div class="col-xs-6">
                    <div class="row">
                        <div class="leader_index col-xs-3 col-xs-9">01</div>
                        <div class="leader_name" id="user_name1">
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="leader_score" id="point1"></div>
                </div>
            </div>

                <div class="row second_leader">
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="leader_index col-xs-3 col-xs-9">02</div>
                            <div class="leader_name"  id="user_name2"></div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="leader_score" id="point2"></div>
                    </div>
                </div>

                <div class="row third_leader">
                <div class="col-xs-6">
                    <div class="row">
                        <div class="leader_index col-xs-3 col-xs-9">03</div>
                        <div class="leader_name" id="user_name3"></div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="leader_score" id="point3"></div>
                </div>
            </div>

                <div class="row normal_leaders">
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="leader_index col-xs-3 col-xs-9" id="num"></div>
                            <div class="leader_name" id="user_name"></div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="leader_score" id="point"></div>
                    </div>
                </div>

        </div>
    </div>
    </div>
@stop

</body>
</html>