<script src="js/profileVali.js"></script>

@extends('layouts.sidebar')
@section('content')
    <div class="row">

        {!! Form::model($users,['method'=> 'POST','url'=>['/profile_save'],'name'=>'profile','files'=>'true','id'=>'pro','enctype'=>'multipart/form-data']) !!}
        {{ csrf_field() }}
        <div class="col-sm-12 header_full_length" style="background-image: url('images/profile_header_bg.jpg');">
            <div class="profie_info_warapper">
                <div class="profile_pic_wrapper">
                    <div class="profile_pic" id="rr" style="background-image: url('{{$users['profile_pic']}}')">
                        <img src="images/pro_pic_edit_tog.png" class="edit_tog">
                    </div>
                </div>
                <input type="hidden" name="user_id" value="{{$users['user_id']}}" />
                <div class="profile_info">
                    <div class="row prp_detail_row">
                        <div class="col-sm-8 col-xs-8">
                            <div class="un_title">Username&nbsp; @if ($errors->has('user_name'))
                                    <span >{{ $errors->first('user_name') }}</span>
                                @endif
                            </div>
                            <div class="detail_un"><input type="text" name="user_name" value="{{$users['user_name']}}" readonly></div>
                        </div>
                        <div class="col-sm-4 col-xs-4 text-right">
                            <img src="images/pencil_icon.png" width="20" class="dr_pencil_icon">
                        </div>
                    </div>
                    @if($users['email']!=null)
                        <div class="row prp_detail_row">
                            <div class="col-sm-8 col-xs-8">
                                <div class="un_title">Email&nbsp;@if ($errors->has('email'))
                                        <span >{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="detail_un"><input type="email" name="email" id="email" value="{{$users['email']}}" readonly></div>
                            </div>
                            <div class="col-sm-4 col-xs-4 text-right">
                                <img src="images/pencil_icon.png" width="20" class="dr_pencil_icon">
                            </div>
                        </div>
                    @endif
                    @if($users['password']!=null)
                        <div class="row prp_detail_row">
                            <div class="col-sm-8 col-xs-8">
                                <div class="un_title">Password&nbsp;@if ($errors->has('password'))
                                        <span >{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="detail_un"><input type="password" name="password" id="password" value="{{$users['password']}}" readonly></div>
                            </div>
                            <div class="col-sm-4 col-xs-4 text-right">
                                <img src="images/pencil_icon.png" width="20" class="dr_pencil_icon">
                            </div>
                        </div>
                    @endif
                    <input type="file" name="file"  id="pro_pic_uploader"/>
                    {{--<input type="file" name="file" value="">--}}
                </div>

            </div>

            <div class="game_info_warapper">
                <div class="row" id="pro_info_save">
                    <div class="col-xs-12 text-right">
                        <button class="pro_info_save_btn" id="edit" type="submit" name="submit">Save</button>
                    </div>
                </div>
                <div class="row game_detail_row_r">
                    <div class="col-sm-8 col-xs-8">
                        <div class="un_title">High Score</div>
                        <div class="detail_un">{{$users['score']}}</div>
                    </div>
                    <div class="col-sm-4 col-xs-4 text-right">
                        <img src="images/medal_icon.png" class="dr_cup_icon">
                    </div>
                </div>
                <div class="row game_detail_row_g">
                    <div class="col-sm-8 col-xs-8">
                        <div class="un_title">Rank</div>
                        <div class="detail_un">{{$users['rank']}}</div>
                    </div>
                    <div class="col-sm-4 col-xs-4 text-right">
                        <img src="images/badge_icon.png" class="dr_badge_icon">
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="row">

        @if(is_null($items[0]))
            <div class="col-sm-12">
                <h2 class="games_played_div">Not Games Played</h2>
            </div>
        @else
            <div class="col-sm-12">
                <h2 class="games_played_div">Games Played</h2>
            </div>
            <div class="col-sm-12">
                <div class="row played_game_block" style="background-image: url('images/played_game_bg.jpg');">
                    @foreach($items as $item)
                        <div class="col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 game_list_wrapper">
                            <div class="row profile_game_list_item played_game_list">
                                <div class="col-sm-4 col-xs-6 profile_g_name_rate">
                                    <h2>{{$item->game->game_name}}</h2>
                                    <div class="game_ele_rate_profile"></div>
                                </div>
                                <div class="col-sm-4 col-xs-6 highest_score text-center">
                                    <h4>Highest Score</h4>
                                    <h2>{{$item->score}}</h2>
                                </div>
                                <div class="col-sm-4 col-xs-12 text-center">
                                    <button class="play_btn_profile" onclick="window.location.href='{{url($item->game->game_url)}}'">Play Game</button>
                                    {{--<a href="{{url($item->game->game_url)}}" class="play_btn_profile" class="header_play_btn">Play Game</a>--}}
                                </div>
                            </div>

                        </div>
                    @endforeach

                </div>
            </div>
        @endif
    </div>
@endsection