
<html>
@extends('layouts.default1')
@section('content')
    <head>

        <script>
            $(document).ready(function(){
                $('.rating').each(function(){
                    var str= $(this).val();
                    console.log(str);

                    $('#rateYo').rateYo({
                        starWidth: "30px",
                        maxValue: 5,
                        rating:str,
                        halfStar: true,
                        readOnly: true,
                        spacing: "5px",
                        normalFill: "#dfdfdf",
                        ratedFill: "#ffde00",
                        starSvg: "<svg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 24 24' style='enable-background:new 0 0 24 24;' xml:space='preserve'>"+
                        "<path d='M12.6,1.9l3,6.3l6.9,1c0.6,0.1,0.8,0.8,0.4,1.2l-5,4.8l1.2,6.9c0.1,0.6-0.5,1-1,0.7L12,19.4l-6.1,3.3 c-0.5,0.3-1.1-0.2-1-0.7l1.2-6.9l-5-4.8C0.6,9.9,0.9,9.2,1.4,9.1l6.9-1l3-6.3C11.6,1.4,12.4,1.4,12.6,1.9z'/>"+
                        "</svg>"
                    });
                });

                var i=0;
                $('.ratings').each(function(){
//                    var i= $(this).attr("id", this.id + index);
                    var strs= $(this).val();
                    console.log(strs);

                    $('#rateYos'+i).rateYo({
                        starWidth: "15px",
                        maxValue: 5,
                        rating:strs,
                        halfStar: true,
                        readOnly: true,
                        spacing: "5px",
                        normalFill: "#dfdfdf",
                        ratedFill: "#ffde00",
                        starSvg: "<svg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 24 24' style='enable-background:new 0 0 24 24;' xml:space='preserve'>"+
                        "<path d='M12.6,1.9l3,6.3l6.9,1c0.6,0.1,0.8,0.8,0.4,1.2l-5,4.8l1.2,6.9c0.1,0.6-0.5,1-1,0.7L12,19.4l-6.1,3.3 c-0.5,0.3-1.1-0.2-1-0.7l1.2-6.9l-5-4.8C0.6,9.9,0.9,9.2,1.4,9.1l6.9-1l3-6.3C11.6,1.4,12.4,1.4,12.6,1.9z'/>"+
                        "</svg>"
                    });
                    i++;
                });
            });
        </script>
    </head>
    <body>
    <form method="get" class="set_margin_0" enctype="multipart/form-data">
        <div class="row">
            <div class="col-sm-12 header_full_length" style="background-image: url('{{$best->game_pic}}');">
                <div class="bg_overlay">
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1 game_info">
                            <h1 class="game_title">{{$best->game_name}}</h1>
                            <p class="short_descri"> {{$best->description}}</p>

                            <input type="hidden" id="grate" class="rating" value="{{$best->rating}}"/>
                            <div id="rateYo"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row home_block_wrapper">
            @foreach($games as $game)
                <div class="col-sm-4 top_game_ele" onclick="window.location = 'description/{{$game->game_id}}';">
                    <input type="hidden" name="game_id" value="{{$game->game_id}}"/>
                    <div class="block_both_sides">
                        <div class="block_front_side" style="background-image: url('{{$game->game_pic}}');">
                            <div class="game_info_blocks">
                                <h1 class="game_title_block">{{$game->game_name}}</h1>
                                <p class="short_descri_block">{{$game->description}}</p>
                                <input type="hidden" id="grates" class="ratings" value="{{$game->rating}}"/>
                                <div id="rateYos{{$loop->index}}"></div>
                            </div>
                        </div>
                        <div class="block_back_side rotate_Y_axis" style="background-image: url('{{$game->game_pic}}');">
                            <div class="game_info_blocks text-center">
                                <h1 class="game_title_block">{{$game->game_name}}</h1>
                                <p class="short_descri_block">{{$game->description}} </p>
                                <div class="game_ele_rate_block"></div>
                                <button class="play_btn_block">Play Game</button>
                            </div>
                        </div>
                    </div>
                </div>
        @endforeach
        </div>

    </form>
    @endsection
    </body>

</html>
