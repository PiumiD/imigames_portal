<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['middleware'=>['web']],function (){

    Route::get('/','GameController@welcome_home')->name('home');


Route::get('/getname','UserController@ajax');

Route::get('/login','GameController@welcome_home');

Route::get('/first',[

    'uses'=>'GameController@home_page',
    'as' =>'first',
    'middleware'=>'auth'
]);

Route::get('/game_description/{game_id}','GameController@game_page');
Route::get('/description/{game_id}','GameController@game_new');

Route::get('/star','RatingController@add_star');
Route::get('/getfewcomment','RatingController@getfewcomment');

Route::get('/profile','UserController@view_profile');
Route::post('/profile_save','UserController@save_profile');


Route::get('/details','LeadController@defaultValue');
Route::get('/leader_board','LeadController@getName');
Route::get('/uid','LeadController@getUser');

Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');

Route::post('/contact','ContactController@save_con');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/welcome', 'GameController@welcome_home');
//Route::get('/logout', 'HomeController@getLogout');

Route::get('/commentform', function () {return view('pages.commentform');});
Route::post('/handle','Comment_LikeController@handle');

});

